// �������-����������� "���������"
// �������� ����������� ��� ��������� � ����� �1
// ��. ����������� ����
function Maze(targetTable) {
    // "����������" ������� - ������� <table> ���������, �� ������� ����� ������� ��� "��������"
    this.targetTable = targetTable;
    // 2D ������ �����. 0 - ��������� ������, 1 - �����, 2 - ����, 3 - �����
    this.cells = [
        [2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 3],
        [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
    this.h = this.cells.length;
    this.w = this.cells[0].length;
    // ������� � �������� 0 - ����� CSS ��� �������� TD ��������� ������ ���������,
    // ������� � �������� 1 - ��� "�����", ...
    // ��� ���������� ��������� ����� ������� ������ ����� ��������, ����������� � �������:
    // var cellClass = this.cellClassNames[this.cells[y][x]];
    this.cellClassNames = ['empty', 'filled', 'enter', 'exit', 'path'];
    // ����� ��, � ������������, ������ ���������� ����� ����� � ������ - ���������� � ����������
    this.findEnterExitPoints();
}

// ��������� "��������" �� ������� (<table>)
// ������� �������� ���� ��������� ��������� � ����� �1 - ��. �����������
Maze.prototype.build = function() {
    for (var y = 0; y < this.h; y++) {
        var row = this.targetTable.insertRow(y);
        for (var x = 0; x < this.w; x++) {
            var mazeCell = this.cells[y][x];
            var cell = row.insertCell(x);

            if (mazeCell < 100) { // ����� � ������ ������ ������ � ��������������� ������
                cell.className = this.cellClassNames[mazeCell];
                cell.innerHTML = '&nbsp;';
            } // ���� � ������ - ����� 100 ��� ����� - ��� ����� ��������� ��� �����
              // ����� ����� ��������������� ����� - ����� �� 100 � ����     
            else cell.innerHTML = mazeCell;
        }
    }
}

// "�������" ������� - ��������� ���� �� ����� ������, �� ����� ������
// ������ ���, "����� �������", �������� �������� �������� ������ ����
Maze.prototype.buildPath = function () {
    // ������ ������ (matrix) � �������� ��� ���������� ������ �����
    var matrix = this.buildWaveMatrix();
    // �� ������� matrix ������ ���� �� ������ �� �����, �� ���� �����������
    // ������� ������ ��������� ������� (this.cells[y][x]) ��� ���������� ���� (����� 4)
    this.buildPathFromCell(matrix);
}

// "���������������" �������������� �������
// ������ ����� �����, ��������� ���������� ��������
// ������ this.cells � ������ ������������ "��������"
Maze.prototype.showWaveMatrix = function () {
    // ������ ������ �������� buildWaveMatrix - �����
    // ������� this.cells, �� � ����������� ������� �����
    // � ��������� ���� �������� ������ this.cells
    this.cells = this.buildWaveMatrix();
    this.build();
}

// ������� ������� - ������� ����� ������� - ������ ����� ������,
// �� �������� � �� ����������
Maze.prototype.cloneMatrix = function() {
    var cpy = [];
    for (var y = 0; y < this.h; y++) {
        var row = [];
        cpy.push(row);
        for (var x = 0; x < this.w; x++)
            row.push(this.cells[y][x]);
    }
    return cpy;
}

// matrix - ����� ������� cells
// �� � ������� matrix � ��������� (�� �� ����) ��������� �������
// ����������� ����� - 100 � �����. �� ���� �������� �������� �������
// ���� ���� �� ����� ������ � ����� �����
Maze.prototype.buildPathFromCell = function (matrix) {
    // ������� ����� �����. �� ���� ����������� � ����� ����� ����� ����� �����
    // ��������� �����������. ������ �� ������� ��� �������� �������� ������� ������
    var minFront = 99999;
    // �������� ��������� ��������������� �� ����� ������
    // ���������� ����� ������ � ����� ��� ������� � ������������ �������
    // ������� findEnterExitPoints
    var x = this.exit.x, y = this.exit.y;

    while (true) { // ����� �� ����� ����� �����������, ����� ����� �������� � ����� �����
        // ������� 4 ��������� ����������� - 4 ������:
        // �����, ������, ���� � ���� ������� [y][x] ������
        var directions = [{ x: x - 1, y: y }, { x: x + 1, y: y }, { x: x, y: y - 1 }, { x: x, y: y + 1 }];
        // �����������, � ������� ����� ������ ��������� ��� - ���� �� �������� ������� directions
        // ���������� �� ������� (null)
        var chosenDirection = null;

        // ������ ��� 4 �����������
        for (var i = 0; i < directions.length; i++) {
            // ������� getFrontInCell �������� �������� � ��������� �����
            // ��������� ����� ���� ���������� �������� - ����� �����...            
            var front = this.getFrontInCell(directions[i].x, directions[i].y, matrix);
            // ... ��������� ����� ���� ����� ������ - ������, �� ��������� � �������� ������, �������������,
            if (front === 2)
                return; // ����� ��������
            // .. �������, �� ����� ��������� � ������ ������. ���� "� �����". ���� - ������ ����� �� �������
            // ���� ���������. � ���� ������ getFrontInCell ������ 0
            if (front === 0) continue;
            // �������� ������ ����� � ���� ����� ������, ��� ������� ��������
            // ������, ��� �������� � ����� �� �������� ������
            if (front < minFront) {
                minFront = front; // ���������� �����, ������� �������� ������ �����
                // �������� ��������������� �� ������� �����������
                chosenDirection = directions[i];
                // ��������, � ��������� ������ �������� ������ ����� ����� ��� ������ -
                // - ����� ���� �� ��������
            }
        }
        // ���� for ... �� 4-� ��������� ������������ ��������, ����������� �������
        x = chosenDirection.x;
        y = chosenDirection.y;
        // �������� ������ � �������� (�� � ������� matrix!) ������� ��� ������ "����" �� ������ �� �����
        this.cells[y][x] = 4;
    } // �������������� �� ��������� ������ ([y][x]) ���������� �����.
}

// ������������ ��� ��������� ���� � ������� buildPathFromCell
// �� ����, ������ ���������� �������� �� ������ matrx[y][x]
// ��� ���� ��������� ��� ��������:
// - ���� ���������� x, y �� ��������� ���� ��������� (< 0 ��� ������ ���� ����� ����������� �������) - ������� 0
// - ���� � ������ matrix[y][x] - ����� ������ � "���������" - ������� ��� �������� (2)
// - ���� � ������ �������� 100 ��� ������ - ������� ��� ��������
// - � ��������� ������ ������� 0
Maze.prototype.getFrontInCell = function (x, y, matrix) {
    if (x < 0 || x >= this.w) return 0;
    if (y < 0 || y >= this.h) return 0;
    var cell = matrix[y][x];
    return cell == 2 ? cell : cell < 100 ? 0 : cell;
}

// ������� ���������� � ������������ � ���������� ���������� ����� ����� � ����� ������
// � �������� this.enter, this.exit
// ������ � ��������? ��� ���������� (this.exit). �������� (���������� ���� ������ ���� "�����" -
// - ����� ��� x, ���� "���������" y?)
Maze.prototype.findEnterExitPoints = function() {
    for (var row = 0; row < this.h; row++)
        for (var col = 0; col < this.w; col++) {
            if (this.cells[row][col] == 2) {
                this.enter = { x: col, y: row };
                // �� ������ ��� �������� ����� �����
                // ���� ����� ������ ��� ������� (this.exit != null) -
                // ����� ��������� �����
                if (this.exit) return;
            }
            if (this.cells[row][col] == 3) {
                this.exit = { x: col, y: row };
                if (this.enter) return;
            }
        }
}
 
// ������� ������� ������� (matrix) � ��������� �� ���������� ������ ���������� "�����"
// ������������ ��� ��������� ���� � "���������" �������� buildPath
Maze.prototype.buildWaveMatrix = function() {
    var cpy = this.cloneMatrix();
    // ������� buildOneWave ��������� ��������� "������" �����
    // � ���������� true, ���� ����� ������ ��� �� ����������
    while (!this.buildOneWave(cpy));
    // ����� ������, ���� ��������
    return cpy;
}

// ������� ���������� ��� ��������� ���� �������� buildWaveMatrix
// ���� ��������� ������, ����� � �������� ���� ���� �� ���� ������, � ������� �������� �������� "�����"
// ��, ��������� ������ �������. 
Maze.prototype.buildOneWave = function (matrix) {
    // ������� ������ ������...
    for (var y = 0; y < this.h; y++)
    for (var x = 0; x < this.w; x++) {
        var cell = matrix[y][x];
        // ... �� ���������� ��� ������ ��������� ������ - ���� - ������ ������ -
        // - ������ ������ (�� ����)
        if (cell == 0) {
            // �������� ����������� �� �������� ������ ����� � �������� �������
            var front = this.getNearestFront(matrix, x, y);
            // ����� ��������� 0 - ����� ����� �� ����� �� �� ����� �� �������� �����, ���� 
            // ��� �������� ������ ������
            if (front != 0) matrix[y][x] = front;
            continue;
        }
        if (cell == 3) { // �� ��������� � ������ ������
            var front = this.getNearestFront(matrix, x, y);
            // ���� ����� ��������� �� ����� �� �������� � ������� ������ ����� - ��������������� ����� ���������
            // ���������� true - ���������� ������ buildOneWave �� �����������
            if (front > 0) return true;
        }
        continue;
    }
    // ������� false - ��������������� ����� ��� �� ���������
    // (�� ����� �� ����� ������)
    return false;
}

// ������������ ��� ���������� ������ ����� � ������� buildOneWave
// ��� ��������� ������ (matrix[y][x]) ���� ����������� �������� ����� � ����� �� 4-� �������� ������
Maze.prototype.getNearestFront = function(matrix, x, y) {
    var fronts = [];
    // ������� ����� ������. ���� ����� - ������ (����� �� ������� ����) �������� � ������ fronts 0
    fronts.push(x == 0 ? 0 : matrix[y][x - 1]);
    // ����������, ���������� � ������ fronts �������� ������� ������ - ��� 0, ���� ��������� � ������� �������
    fronts.push(y == 0 ? 0 : matrix[y - 1][x]);
    fronts.push(x == this.w - 1 ? 0 : matrix[y][x + 1]);
    fronts.push(y == this.h - 1 ? 0 : matrix[y + 1][x]);
 
    // minFront - �������� ������ �����, ������� ����� �������� � �������
    // matrix[y][x] ������ - ��� 0, ���� ����� �� ����� �� �������� �����
    var minFront = 0;
    for (var i = 0; i < fronts.length; i++) {
        // ���� �������� fronts[i] (�������� �� ������ �����, ������, ������ ��� ����� �� ������� ������)
        // ����� 2 - ����� ����� - ������� ����� ����� 100, ��� ���
        // ����� ����� ���������� �� 100, ��� ��������, ����� �� ������ � ������� ���������� �����
        //
        // ���� � ������� ������ (�������� fronts[i]) ��� ����� (�� 100 � ����) - �������� � ����������
        // front 0
        var front = fronts[i] >= 100 ? fronts[i] + 1 : fronts[i] == 2 ? 100 : 0;
        if (front != 0) // �������� 0 ��� �� ����������
            if (minFront == 0 || front < minFront) minFront = front; 
    }
    // minFront ��� �� 1 ������ ������������ �������� � �������� ������... ��� - ����� 0
    return minFront;
}