function spoilerize() {
    $(".spoiler").each(function (i, p) {
        $(p).hide();
    });
    $(".spoiler").before('<input type="button" value="&nbsp;SPOILER&nbsp;" onclick="switchSpoiler()" /> <hr/>');
}

function switchSpoiler() {
    var sender = $(event.target);
    var textBlock = sender.next().next();
    textBlock.toggle();
}

$(function() {
    spoilerize();
});